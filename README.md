# StaffGraph

This project can help companies to predict the KPI of their staff and
optimize the overall KPI. We use statistics received from the company,
a graph of social connections in the community, and the emotional status
of an employee.

We are using:
- pretrained net to recognize the emotion of employee
- *openface* to detect and classify faces
- torch-geometric implementation of the convolution graph networks for
  classification of the performance.

1. Install and check requirements
```git clone https://gitlab.com/surikn/StaffGraph && cd StaffGraph```
   
2. ```
    docker-compose build
    docker-compose up
   ```
3. Put necessary files in shared directory. 
   - Pictures of faces of your staff - in ```shared/faces/training-images```
   - Social networking information in ```graphNN/data/edge_list.pt```. 
     There you should use pairs of indexes to represent social connection.
   
4. You can make request directly in API of each service, or run inference in graphNN directly
   