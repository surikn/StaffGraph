from __future__ import print_function
from flask import Flask, request
import subprocess
import zipfile
import uuid
import os

app = Flask(__name__)

SCRIPTS_PATH = "/root/openface/util"
BATCH_REPRESENT_PATH = "/root/openface/batch-represent"
DEMOS_PATH = "/root/openface/demos/"

TRAINING_IMAGES = "/data/training-images"
ALIGNED_IMAGES = "/data/aligned-images"
EMBEDDINGS_DIR = "/data/embeddings"
ZIP_STORAGE = "/data/zips"

TEST_DIR = "/data/test"


@app.route("/alignImages", methods=["GET"])
def align_images():
    alignments = subprocess.call([" ".join([
        os.path.join(SCRIPTS_PATH, "align-dlib.py"),
        os.path.join(TRAINING_IMAGES),
        "align",
        "outerEyesAndNose",
        ALIGNED_IMAGES,
        "--size", "96"
    ])], shell=True)

    return {"ok": True}


@app.route("/generateRepresentations", methods=["GET"])
def generate_representations():
    representations = subprocess.call([" ".join([
        os.path.join(BATCH_REPRESENT_PATH, "main.lua"),
        "-outDir",
        EMBEDDINGS_DIR,
        "-data",
        ALIGNED_IMAGES
    ])], shell=True)
    return {"ok": True}


@app.route("/trainClassifier")
def train_classifier():
    seb_res = subprocess.call([" ".join([
        os.path.join(DEMOS_PATH, "classifier.py"),
        "train",
        EMBEDDINGS_DIR
    ])], shell=True)
    return {"ok": True}
    

@app.route("/uploadImages", methods=["GET", "POST"])
def upload_images():
    def unzip_file(zip_src, dst_dir):
        r = zipfile.is_zipfile(zip_src)
        if r:
            fz = zipfile.ZipFile(zip_src, "r")
            for inner_file in fz.namelist():
                fz.extract(inner_file, dst_dir)
        else:
            print("Please upload zip file")

    if request.method == "POST":
        if "file" not in request.files:
            return {"ok": False, "message": "Invalid request"}
        file = request.files["file"]

        if file.filename == "":
            return {"ok": False, "message": "Empty file"}

        zip_name = str(uuid.uuid4())
        zip_path = os.path.join(ZIP_STORAGE, zip_name + ".zip")
        file.save(zip_path)

        dest_path = os.path.join(os.path.join(TRAINING_IMAGES, zip_name))
        os.mkdir(dest_path)
        unzip_file(zip_path, dest_path)
        os.remove(zip_path)

        return {"ok": True, "token": zip_name}
    return {"ok": False}


@app.route("/infer", methods=["POST"])
def infer():
    if request.method == "POST":
        if "image" not in request.files:
            return {"ok": False, "message": "Invalid request"}
        image = request.files["image"]

        if image.filename == "":
            return {"ok": False, "message": "Empty file"}

        image_name = str(uuid.uuid4())
        image_ext = image.filename.split(".")[-1]
        image_path = os.path.join(TEST_DIR, image_name + image_ext)
        image.save(image_path)

        inference = subprocess.check_output([" ".join([
            os.path.join(DEMOS_PATH, "classifier.py"),
            "infer",
            os.path.join(EMBEDDINGS_DIR, "classifier.pkl"),
            image_path,
        ])], shell=True)

        os.remove(image_path)
        return {"ok": True, "infer": inference}
    return {"ok": False}


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9090)
