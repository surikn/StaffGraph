from flask import Flask, request
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import backend as K
import tensorflow as tf
import cv2
import numpy as np
import subprocess
import zipfile
import pickle
import uuid
import os

app = Flask(__name__)

IMG_PATH = "/data/faces"
PRED_PATH = "/data/preds"


def get_model():
    model = Sequential()

    model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(48, 48, 1)))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5, name='intermediate'))
    model.add(Dense(7, activation='softmax'))
    return model


def predict(image_path):
    model = get_model()
    model.load_weights('/code/flask-server/model.h5')

    get_inter_layer_output = K.function([model.layers[0].input],
                                      [model.get_layer('intermediate').output])
    # prevents openCL usage and unnecessary logging messages
    cv2.ocl.setUseOpenCL(False)

    emotion_dict = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}

    frame = cv2.imread(image_path)
    print("FRAME: ", frame)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cropped_img = np.expand_dims(np.expand_dims(cv2.resize(gray, (48, 48)), -1), 0)
    prediction = model.predict(cropped_img)
    dist = get_inter_layer_output([cropped_img])[0]
    maxindex = int(np.argmax(prediction))
    return {"distribution": dist, "class": emotion_dict[maxindex]}


@app.route("/infer", methods=["POST"])
def infer():
    if request.method == "POST":
        if "image" not in request.files:
            return {"ok": False, "message": "Invalid request"}
        image = request.files["image"]

        if image.filename == "":
            return {"ok": False, "message": "Empty file"}

        image_name = str(uuid.uuid4())
        image_ext = image.filename.split(".")[-1]
        image_path = os.path.join(IMG_PATH, f"{image_name}.{image_ext}")
        image.save(image_path)

        predictions = predict(image_path)
        with open(os.path.join(PRED_PATH, f"{image_name}.pickle"), 'wb') as handle:
            pickle.dump(predictions, handle, protocol=pickle.HIGHEST_PROTOCOL)

        os.remove(image_path)
        return {"ok": True, "predictions": f"{image_name}.pickle"}
    return {"ok": False}


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
